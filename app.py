from flask import Flask, request, render_template, url_for, session, \
                    flash, redirect
from flask_oauth import OAuth
from flask_mongoengine import MongoEngine, Document
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

app = Flask(__name__)

app.config['MONGODB_SETTINGS'] = {
    'db': 'twittertesting', #add your dbname
    'host': 'mongodb://arunkn28:global123@ds137581.mlab.com:37581/twittertesting' #change to your host
}

app.secret_key = '1236830*&^%^asdjlk'

db = MongoEngine(app)
login_manager = LoginManager()
login_manager.init_app(app)
oauth = OAuth()

twitter = oauth.remote_app(
            'twitter',
            consumer_key='Hdl7LMmGEMjKw4uEePXgNDPSd', #Add your customer key
            consumer_secret='lkz6MTBFPfwx45grTzEEhEo7Zm8iYDAm2dkzZgfb1A8tv5ZPxZ', #add customer secret key
            base_url='https://api.twitter.com/1.1/',
            request_token_url='https://api.twitter.com/oauth/request_token',
            access_token_url='https://api.twitter.com/oauth/access_token',
            authorize_url='https://api.twitter.com/oauth/authorize'
            )


class User(UserMixin, db.Document):
    meta = {'collection': 'users'} # change the collecteions name of you want
    username        = db.StringField(max_length=50)
    email           = db.StringField(max_length=50)
    password        = db.StringField()
    twitter_user_id = db.StringField()

@login_manager.user_loader
def load_user(user_id):
    return User.objects(pk=user_id).first()

@twitter.tokengetter
def get_twitter_token(token=None):
    return session.get('twitter_token')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['POST','GET'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    if request.form.get('email') and request.form.get('password'):
        check_user = User.objects(email=request.form.get('email')).first()
        if check_user:
            if check_password_hash(check_user['password'],request.form.get('password')):
                login_user(check_user)
                session['user_name'] = check_user['username']
                print check_user['username']
                print check_user['email']
                return render_template('index.html')
        return render_template('index.html',context='Username password did not match')    
    return twitter.authorize(callback=url_for('oauth_authorized',
        next=request.args.get('next') or request.referrer or None))

@app.route('/register', methods=['POST','GET'])
def register():
    username        = request.form.get('username')
    email           = request.form.get('email')
    password        = request.form.get('password')
    error_msg=''
    if request.method == 'POST':
        if username and email \
            and password:
            existing_user = User.objects(email=email).first()
            if not existing_user:
                hashed_password = generate_password_hash(password, method='sha256')
                user = User(username,email,hashed_password,'').save()
                login_user(user)
                session['user_name'] = username
                return render_template('index.html',context=error_msg)
            else:
                error_msg='User already exists'
    return render_template('register.html',context=error_msg)

@app.route('/logout', methods = ['GET'])
@login_required
def logout():
    logout_user()
    session['user_name']=None
    return redirect(url_for('index'))
        
@app.route('/oauth-authorized')
@twitter.authorized_handler
def oauth_authorized(resp):
    next =request.args.get('next') or url_for('index')
    if not resp:
        flash('Denied Access')
        return redirect(next)
    access_token = resp['oauth_token']
    if access_token:
        user = User(resp['screen_name'],'','',resp['user_id']).save()
        login_user(user)
        session['access_token'] = access_token
        session['user_name'] = resp['screen_name']
        print resp['screen_name']
        print resp['user_id']
        session['twitter_token'] = (
                    resp['oauth_token'],
                    resp['oauth_token_secret']
            )
        #user_details = twitter.request('account/verify_credentials.json') --> use this to get user email when your website is set
        #user_deatils has all user related data..you can use this for your need
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run()
